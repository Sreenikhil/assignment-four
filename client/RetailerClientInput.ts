import {GraphQLInt, GraphQLString} from "graphql";


export interface RetailerClientInput{
    Company_name: string,
    Country_code: number,
    Phone_Number: number,
    Address: string,
    Country: string,
    City: string,
    Retailer_ID: string,
    password: string,
}