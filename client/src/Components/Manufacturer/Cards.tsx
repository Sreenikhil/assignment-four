import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
        border: 0,
        borderRadius: 3,
        borderColor: 'blue',
        width: 350,
        height: 150,
        boxShadow: '0 3px 5px 2px rgba(102, 102, 255, .4)',
        marginLeft: 400,
        marginTop: 50,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: "#fffff0"
    },

    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

interface Iprops{
    MedicineID ?: number,
    Count?: number,
    Company?: string,
    Note?: string
    Duration_in_days?: number,
    Cost?: number,
    actor?: string,
    Retailer?: string

}


export const Cards: React.FC<Iprops> = (props: Iprops)=> {
    const classes = useStyles();

  if(props.actor === "Manufacturer"){
    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>

                <Typography >
                    Medicine ID: {props.MedicineID}
                </Typography>

                <Typography>
                   Count:  {props.Count}
                </Typography>

                <Typography >
                   Company:  {props.Company}
                </Typography>

                <Typography >
                    Note:   {props.Note}
                </Typography>

                <Typography >
                    Retailer ID:   {props.Retailer}
                </Typography>

            </CardContent>

        </Card>
    )
  }

      return (
          <Card className={classes.root} variant="outlined">
              <CardContent>
                  <Typography>
                      Duration_in_days : {props.Duration_in_days}
                  </Typography>

                  <Typography>
                      Cost: {props.Cost}
                  </Typography>

                  <Typography>
                      Medicine ID: {props.MedicineID}
                  </Typography>

                  <Typography>
                      Note: {props.Note}
                  </Typography>

              </CardContent>

          </Card>
      )

}

