import React, {useEffect, useState} from 'react'
import {HomeNavigation} from "./HomeNavigation";
import TextField from "@material-ui/core/TextField/TextField";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Axios from 'axios'
import {useMutation, useQuery} from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import {useSelector} from "react-redux";
import {IRetailerReducer} from "../IRetailerReducer";
import {useHistory} from "react-router-dom";
import {Cards} from "./Manufacturer/Cards"



// If there are any props, I will write it later
// The design of the web page is to have a form, which takes, Medicine Id and the count, the use wants
// We can get retailer ID using redux


const useStyles = makeStyles((theme) =>({
    root:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        padding: '5rem',
        backgroundColor: '#fed8b1',
        marginLeft: 250,
        marginRight: 250
    },
    field:{
        margin: theme.spacing(1),
        width: '30ch',
    },
    userButton:{
        marginTop: '1rem',
        marginRight: '3rem'
    },
    loginButton:{
        marginTop: '1rem'
    },
    inroot:{
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing( 0, 0, 1, 0),
    },
}))



export const NotifyManufacturer: React.FC = ()=> {
    const classes = useStyles();
    const history = useHistory();
    const [id, setId] = useState(0)
    const [unique, setUnique] = useState('')
    const [count, setCount] = useState(0)
    const [note, setNote] = useState('')
    const [company, setCompany] = useState('')
    const [websocket, setwebsocket] = useState<WebSocket>(Object)
    const [Rmessage, setRMessage] = useState<string[]>([])


    const retailerId: string | undefined = useSelector<IRetailerReducer, string | undefined>(state => state.RetailerReducer.Retailer_ID)

    function idChange(event: React.ChangeEvent<HTMLInputElement>) {
        setId(parseInt(event.target.value))
    }

    function countChange(event: React.ChangeEvent<HTMLInputElement>) {
        setCount(parseInt(event.target.value))
    }


    function noteChange(event: React.ChangeEvent<HTMLInputElement>) {
        setNote(event.target.value)
    }

    async function submitRequest() {
        // For the backend we will send company name:
        const response = await Axios.get("/Company/"+ retailerId)
        // Check if Medicine exists in the database
        const new_response = await Axios.get("/Medicine/"+ id + "/" + retailerId )

       const {Company_name} = response.data
        const reqObj = {
            MedicineID: id,
            MedicineCount: count,
            id: unique,
            Company_name: Company_name,
            Notes: note
        }
        const {msge} = new_response.data
        if(new_response.status === 200 && !msge) {
            // We are converting to json and sending it. In Axios we do not because Axios does that for us
            websocket.send(JSON.stringify(reqObj))

            alert("Data is sent to the manufacturer")
        }
        else if(new_response.status === 200 && msge === "Not Found"){
            alert("Medicine Does Not Exists")
        }

    }


    // The most basic logic is, I want to open websocket as soons as I launch the notify medicine
    useEffect(() => {
        // I will create an unique ID here and parse it in the server
        // If it is client then we will go for 10 digit unique id else server
        const chars = "'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"
        let result = '';
        // Note: As you said, I will use nanoid() next time, when I encounter the same situation
        /* const k = nanoid(10)
        I will write the logic here:

         */
        for (let i = 11; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];

        const uniqueId: string = result
        setUnique(uniqueId)
        const webSocket = new WebSocket('ws://localhost:5500/'+ uniqueId);
        (async () => {
            console.log("I am in the component did mount function")
            console.log("I have set a websocket")
            setwebsocket(webSocket)
            webSocket.onmessage = (message: MessageEvent) => {
                console.log('Received message: ', message);
                console.log("Exact Message is: ", typeof message.data)
                const k: string = message.data
                console.log("Message is: "+ k)
                const new_obj = JSON.parse(k)

                // I am converting exact message to an object
                setRMessage((oldMessages:String[]) => [...oldMessages, message.data])
                console.log("I am setting the messages array")
            };
        })()
        return () => webSocket.close();
        // I am guessing webSocket and websocket will point to same location

    }, [])

    return (
        <div>
            <HomeNavigation/>
            <div className={classes.root}>
                <TextField className={classes.field} id="outlined" margin="normal" label="Medicine-ID"
                           variant="outlined" size="medium"
                           onChange={idChange}/>

                <TextField className={classes.field} id="outlined" margin="normal" label="count"
                           variant="outlined" size="medium"
                           onChange={countChange}
                />

                <TextField className={classes.field} id="outlined" margin="normal" label="Note"
                           variant="outlined" size="medium"
                           onChange={noteChange}
                />

                <Button className={classes.userButton} onClick={submitRequest} variant="contained" color="secondary">
                    Notify
                </Button>
            </div>
         <h1> Responses from the Manufacturer are:  </h1>

            {
                Rmessage?.map((msg: string) => {
                    console.log("I am here")
                    console.log("The type of received message is"+ typeof msg)
                    const k: string = msg
                    console.log("The value of the message is: "+ msg)
                    const new_obj = JSON.parse(k)
                    console.log(new_obj)
                    console.log("I a here")
                    return(
                        <div>
                        <h1> I am fine </h1>

                    <Cards Duration_in_days={new_obj.Duration}
                           Cost = {new_obj.Cost}
                           Note = {new_obj.Notes}
                           MedicineID = {new_obj.Medicine_ID}
                           actor = {"Client"}/>
                    </div>
                )
                })
            }

        </div>
    )
}