
// As professor said,
export enum ERetailer  {
    CHANGE_RETAILER = "CHANGE_RETAILER"
}

export enum EDestroyer{
    DESTROY_RETAILER= "DESTROY_RETAILER"
}

export function RetailerAction(Retailer_ID: string){
    return {
        type: ERetailer.CHANGE_RETAILER,
        Retailer_ID
    }
}

export function DestroyAction() {
    return{
        type: EDestroyer.DESTROY_RETAILER
    }
}

// There you go!! I have successfully created an Action in the beautiful concept of redux
// Time stamp: 04/21/2020 5:19 PM. On a couch in New Jersey