"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styles_1 = require("@material-ui/core/styles");
var react_router_dom_1 = require("react-router-dom");
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 150,
        border: 5,
        borderColor: 'black',
        backgroundColor: 'gray',
        marginLeft: 300,
        marginRight: 250,
        width: "50rem",
        height: "20rem"
    },
}); });
exports.About = function () {
    var classes = useStyles();
    var history = react_router_dom_1.useHistory();
    function GoLogin() {
        history.push('/');
    }
    return (<div style={{ textAlign: 'center' }} className={classes.root}>
            <p>  Pharma Pharma Solutions is an application
                to solve the global problem of lack of medicines at
                right time to the people It
            It brings retailers and manufacturers in the pharma industry under one roof <br />
                Retailers keeps an eye on the stock and request manufacturers when they need <br />
                Manufacturers checks their production and based on their availability they send response to retailer <br />
                Retailers receives responses from all the manufacturers collaborating with pharma solutions and order from the manufacturer of their choice
                    </p>

            <Button_1.default variant="contained" color="secondary" onClick={GoLogin}> Return to Login </Button_1.default>
            </div>);
};
