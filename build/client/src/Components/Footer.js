"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var styles_1 = require("@material-ui/core/styles");
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        padding: '0.3rem',
        backgroundColor: 'gray',
        marginTop: '8rem'
    },
    text: {
        marginLeft: '40rem',
    }
}); });
exports.Footer = function () {
    var classes = useStyles();
    return (<div className={classes.root}>
            <p className={classes.text}> &copy; 2020 Pharma Solutions </p>
        </div>);
};
