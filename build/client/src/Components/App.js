"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_router_dom_1 = require("react-router-dom");
var Login_1 = require("./Login");
var Signup_1 = require("./Signup");
var About_1 = require("./About");
function App() {
    return (<div>
           <react_router_dom_1.BrowserRouter>
               <switch>
                   <react_router_dom_1.Route exact path="/" component={Login_1.Login}/>
                   <react_router_dom_1.Route exact path="/Signup" component={Signup_1.Signup}/>
                   <react_router_dom_1.Route exact path="/About" component={About_1.About}/>
               </switch>
           </react_router_dom_1.BrowserRouter>
           </div>);
}
exports.default = App;
