"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var styles_1 = require("@material-ui/core/styles");
var react_router_dom_1 = require("react-router-dom");
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        border: 10,
        alignItems: 'center',
        borderColor: 'black',
        padding: '3rem',
        backgroundColor: 'gray',
        marginLeft: '10rem',
        marginRight: '10rem',
        marginTop: '5rem'
    },
    field: {
        margin: theme.spacing(1),
        width: '30ch',
    },
    inroot: {
        display: 'flex',
        flexDirection: 'row',
        margin: theme.spacing(0, 0, 1, 0),
    },
    codefield: {
        margin: theme.spacing(1, 1, 0, 17),
        width: '10ch'
    },
    phonefield: {
        margin: theme.spacing(1, 2, 0),
        width: '30ch',
    },
    // (top, right, bottom, left) Clockwise
    country: {
        margin: theme.spacing(1, 1, 0, 43),
        width: '30ch'
    },
    city: {
        margin: theme.spacing(1, 3, 0),
        width: '30ch',
    },
    userButton: {
        marginTop: '1rem',
        marginRight: '3rem'
    },
    signUpButton: {
        marginTop: '1rem'
    }
}); });
exports.Signup = function () {
    var history = react_router_dom_1.useHistory();
    var classes = useStyles();
    var _a = react_1.useState(''), companyName = _a[0], setCompanyName = _a[1];
    var _b = react_1.useState(0), countryCode = _b[0], setCountryCode = _b[1];
    var _c = react_1.useState(0), phoneNumber = _c[0], setPhoneNumber = _c[1];
    var _d = react_1.useState(''), address = _d[0], setAddress = _d[1];
    var _e = react_1.useState(''), country = _e[0], setCountry = _e[1];
    var _f = react_1.useState(''), city = _f[0], setCity = _f[1];
    var _g = react_1.useState(''), retailerID = _g[0], setRetailerID = _g[1];
    var _h = react_1.useState(''), password = _h[0], setPassword = _h[1];
    function Exists() {
        history.push('/');
    }
    function doSignUp() {
        console.log("The information that I got is: ");
        console.log("Company name is: " + companyName);
        console.log("Country Code is: " + countryCode);
        console.log("Phone number is: " + phoneNumber);
        console.log("Address is: " + address);
        console.log("Country is: " + country);
        console.log("City is: " + city);
        console.log("Retailer ID is: " + retailerID);
        console.log("password is: " + password);
    }
    function companyChange(event) {
        setCompanyName(event.target.value);
    }
    function countryCodeChange(event) {
        setCountryCode(parseInt(event.target.value));
    }
    function phoneNumberChange(event) {
        setPhoneNumber(parseInt(event.target.value));
    }
    function addressChange(event) {
        setAddress(event.target.value);
    }
    function countryChange(event) {
        setCountry(event.target.value);
    }
    function cityChange(event) {
        setCity(event.target.value);
    }
    function retailerIDChange(event) {
        setRetailerID(event.target.value);
    }
    function passwordChange(event) {
        setPassword(event.target.value);
    }
    return (<div className={classes.root}>
        <TextField_1.default className={classes.field} id="outlined" margin="normal" label="Company Name" variant="outlined" size="medium" onChange={companyChange}/>

        <div className={classes.inroot}>
        <TextField_1.default className={classes.codefield} id="outlined" margin="normal" label="Code" variant="outlined" size="medium" onChange={countryCodeChange}/>
        <TextField_1.default className={classes.phonefield} id="outlined" margin="normal" label="Phone Number" variant="outlined" size="medium" onChange={phoneNumberChange}/>
        </div>

        <TextField_1.default className={classes.field} id="outlined" margin="normal" label="Address" variant="outlined" size="medium" onChange={addressChange}/>

        <div className={classes.inroot}>
        <TextField_1.default className={classes.country} id="outlined" margin="normal" label="Country" variant="outlined" size="medium" onChange={countryChange}/>
        <TextField_1.default className={classes.city} id="outlined" margin="normal" label="City" variant="outlined" size="medium" onChange={cityChange}/>
        </div>

        <TextField_1.default className={classes.field} id="outlined" margin="normal" label="Retailer-ID" variant="outlined" size="medium" onChange={retailerIDChange}/>

        <TextField_1.default className={classes.field} id="outlined" margin="normal" label="password" type="password" variant="outlined" size="medium" onChange={passwordChange}/>
        <div className={classes.inroot}>
            <Button_1.default className={classes.userButton} onClick={Exists} variant="contained" color="secondary">
                Already an user
            </Button_1.default>

            <Button_1.default className={classes.signUpButton} onClick={doSignUp} variant="contained" color="secondary">
                SignUp
            </Button_1.default>


       </div>

        </div>);
};
