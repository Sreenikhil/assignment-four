"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var styles_1 = require("@material-ui/core/styles");
var react_router_dom_1 = require("react-router-dom");
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        border: 10,
        borderColor: 'black',
        padding: '0.3rem',
        backgroundColor: 'gray',
        justifyContent: 'space-around'
    },
    button: {
        width: '9rem',
        height: '3rem',
        marginTop: '1rem',
    },
    text: {
        marginLeft: '28rem',
    }
}); });
exports.Navigation = function () {
    var classes = useStyles();
    var history = react_router_dom_1.useHistory();
    function GoAbout() {
        history.push('/About');
    }
    return (<div className={classes.root}>
         <h1 className={classes.text}> Pharma Solutions </h1>
         <Button_1.default className={classes.button} onClick={GoAbout} variant="contained" color="secondary"> About </Button_1.default>
         </div>);
};
