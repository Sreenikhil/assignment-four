"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var RetailerMutation_1 = __importDefault(require("./Mutations/RetailerMutation"));
var RetailerQuery_1 = __importDefault(require("./Query/RetailerQuery"));
// Specify a GraphQL schema with our queries ('getters') and mutators ('setters')
exports.default = new graphql_1.GraphQLSchema({
    query: RetailerQuery_1.default,
    mutation: RetailerMutation_1.default
});
