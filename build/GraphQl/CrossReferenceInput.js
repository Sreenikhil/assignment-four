"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var NewMedicine_1 = require("./NewMedicine");
exports.CrossReferenceInput = new graphql_1.GraphQLInputObjectType({
    name: 'CrossReferenceInput',
    description: 'Input fields for the Cross Reference model',
    fields: function () { return ({
        Retailer_ID: { type: graphql_1.GraphQLString },
        ID: { type: new graphql_1.GraphQLList(NewMedicine_1.GraphQlNewMedicine) },
    }); },
});
