"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.SessionsInput = new graphql_1.GraphQLInputObjectType({
    name: 'Session Input',
    description: 'Input fields for the Session model',
    fields: function () { return ({
        Session_ID: { type: graphql_1.GraphQLString },
        Retailer_ID: { type: graphql_1.GraphQLString },
    }); },
});
