"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var retailerSchema_1 = require("../../Models/retailerSchema");
var Retailer_1 = require("../Retailer");
var RetailerInput_1 = require("../RetailerInput");
var Medicine_1 = require("../Medicine");
var MedicineInput_1 = require("../MedicineInput");
var medicineSchema_1 = require("../../Models/medicineSchema");
var bcrypt = require("bcrypt");
// Root mutations specifies all the ways in which your GraphQl endpoint can create/modify (or mutate) data
exports.default = new graphql_1.GraphQLObjectType({
    name: 'RootMutations',
    description: 'Root level mutator resolvers',
    // Here, fields represents the 'setter functions' you can call with your GraphQl endpoint to 'mutate' various data
    fields: function () { return ({
        // Specify an endpoint to create a new User and save them in MongoDB
        createRetailer: {
            type: new graphql_1.GraphQLList(Retailer_1.GraphQlRetailer),
            description: 'Creates a new user',
            // Here is where we specify those UserInput arguments
            args: {
                input: { type: RetailerInput_1.RetailerInput }
            },
            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
            resolve: function (parentValue, _a) {
                var _b = _a.input, Company_name = _b.Company_name, Country_code = _b.Country_code, Phone_Number = _b.Phone_Number, Address = _b.Address, Country = _b.Country, City = _b.City, Retailer_ID = _b.Retailer_ID, Password = _b.Password;
                return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                console.log("The password is: " + Password);
                                // I am storing hash of the password, not the plain text
                                return [4 /*yield*/, bcrypt.hash(Password, 12, function (err, hash) { return __awaiter(void 0, void 0, void 0, function () {
                                        var retailer_exixst;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    if (!err) return [3 /*break*/, 1];
                                                    console.log("Problem in doing hash" + hash);
                                                    return [3 /*break*/, 4];
                                                case 1: return [4 /*yield*/, retailerSchema_1.Retailer.findOne({ $or: [
                                                            { Company_name: { $eq: Company_name } },
                                                            { Retailer_ID: { $eq: Retailer_ID } }
                                                        ] })];
                                                case 2:
                                                    retailer_exixst = _a.sent();
                                                    if (!!retailer_exixst) return [3 /*break*/, 4];
                                                    console.log(hash);
                                                    Password = hash;
                                                    return [4 /*yield*/, retailerSchema_1.Retailer.create({
                                                            Company_name: Company_name,
                                                            Country_code: Country_code,
                                                            Phone_Number: Phone_Number,
                                                            Address: Address,
                                                            Country: Country,
                                                            City: City,
                                                            Retailer_ID: Retailer_ID,
                                                            Password: Password
                                                        })];
                                                case 3: return [2 /*return*/, _a.sent()];
                                                case 4: return [2 /*return*/];
                                            }
                                        });
                                    }); })];
                            case 1:
                                // I am storing hash of the password, not the plain text
                                _c.sent();
                                return [2 /*return*/];
                        }
                    });
                });
            },
        },
        createMedicine: {
            type: Medicine_1.GraphQlMedicine,
            description: 'Creates a new medicine',
            // Here is where we specify those UserInput arguments
            args: {
                input: { type: MedicineInput_1.MedicineInput }
            },
            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
            resolve: function (parentValue, _a) {
                var _b = _a.input, Medicine_name = _b.Medicine_name, count = _b.count, Medicine_ID = _b.Medicine_ID;
                return medicineSchema_1.Medicine.create({ Medicine_name: Medicine_name, count: count, Medicine_ID: Medicine_ID });
            },
        },
    }); }
});
