"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var medicineSchema_1 = require("../../Models/medicineSchema");
var Medicine_1 = require("../Medicine");
var MedicineInput_1 = require("../MedicineInput");
var Retailer_1 = require("../Retailer");
var RetailerInput_1 = require("../RetailerInput");
var retailerSchema_1 = require("../../Models/retailerSchema");
exports.default = new graphql_1.GraphQLObjectType({
    name: 'RootMutations',
    description: 'Root level mutator resolvers',
    // Here, fields represents the 'setter functions' you can call with your GraphQl endpoint to 'mutate' various data
    fields: function () { return ({
        // Specify an endpoint to create a new User and save them in MongoDB
        createMedicine: {
            type: Medicine_1.GraphQlMedicine,
            description: 'Creates a new medicine',
            // Here is where we specify those UserInput arguments
            args: {
                input: { type: MedicineInput_1.MedicineInput }
            },
            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
            resolve: function (parentValue, _a) {
                var _b = _a.input, Medicine_name = _b.Medicine_name, count = _b.count, Medicine_ID = _b.Medicine_ID;
                return medicineSchema_1.Medicine.create({ Medicine_name: Medicine_name, count: count, Medicine_ID: Medicine_ID });
            },
        },
        createRetailer: {
            type: Retailer_1.GraphQlRetailer,
            description: 'Creates a new user',
            // Here is where we specify those UserInput arguments
            args: {
                input: { type: RetailerInput_1.RetailerInput }
            },
            // How should GraphQL handle creating a user? Save the User with the UserInput args with Mongoose
            resolve: function (parentValue, _a) {
                var _b = _a.input, Company_name = _b.Company_name, Country_code = _b.Country_code, Phone_Number = _b.Phone_Number, Address = _b.Address, Country = _b.Country, City = _b.City, Retailer_ID = _b.Retailer_ID, password = _b.password;
                return retailerSchema_1.Retailer.create({ Company_name: Company_name, Country_code: Country_code, Phone_Number: Phone_Number, Address: Address, Country: Country, City: City, Retailer_ID: Retailer_ID, password: password });
            },
        },
    }); },
});
