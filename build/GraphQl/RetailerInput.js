"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.RetailerInput = new graphql_1.GraphQLInputObjectType({
    name: 'RetailerInput',
    description: 'Input fields for the Retailer model',
    fields: function () { return ({
        Company_name: { type: graphql_1.GraphQLString },
        Country_code: { type: graphql_1.GraphQLInt },
        Phone_Number: { type: graphql_1.GraphQLInt },
        Address: { type: graphql_1.GraphQLString },
        Country: { type: graphql_1.GraphQLString },
        City: { type: graphql_1.GraphQLString },
        Retailer_ID: { type: graphql_1.GraphQLString },
        Password: { type: graphql_1.GraphQLString },
    }); },
});
