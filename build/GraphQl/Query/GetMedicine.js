"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var medicineSchema_1 = require("../../Models/medicineSchema");
var Medicine_1 = require("../Medicine");
// Root queries specifies all the ways in which your GraphQl endpoint can return data
exports.default = new graphql_1.GraphQLObjectType({
    name: 'RootQuery',
    description: 'Root level query resolvers',
    // Here, fields represents all the 'getter functions' we can call from our GraphQL endpoint to get data
    fields: function () { return ({
        Retailer: {
            type: new graphql_1.GraphQLList(Medicine_1.GraphQlMedicine),
            resolve: function (parentValue, _a) {
                var MedicineID = _a.MedicineID;
                return medicineSchema_1.Medicine.findOne({ Medicine_ID: { $eq: MedicineID } });
            }
        }
    }); }
});
