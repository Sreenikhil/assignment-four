"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var MedicineRequestSchema = new mongoose.Schema({
    MedicineName: {
        type: String,
        required: true,
    },
    MedicineCount: {
        type: Number,
        required: true // Built-In validation
    },
    MedicineID: {
        type: Number,
        required: true
    },
    RetailerID: {
        type: Number,
        required: true
    },
    by: {
        type: String,
        required: true
    }
});
exports.MedicineRequest = mongoose.model('Request', MedicineRequestSchema);
