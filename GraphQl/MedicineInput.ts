import {GraphQLInputObjectType, GraphQLString, GraphQLInt} from "graphql";


export const MedicineInput = new GraphQLInputObjectType({
    name: 'MedicineInput',
    description: 'Input fields for the Medicine model',
    fields: () => ({
        Medicine_name: {type: GraphQLString},
        count: {type: GraphQLInt},
        Medicine_ID: {type: GraphQLInt}
    }),
})