import {GraphQLList, GraphQLObjectType, GraphQLInt, GraphQLString} from "graphql";
import {Retailer} from "../../Models/retailerSchema";
import {GraphQlRetailer} from "../Retailer";
import {GraphQlMedicine} from "../Medicine";
import {Medicine} from "../../Models/medicineSchema";
import {CrossReference} from "../../Models/CrossReferenceSchema";
import {GraphQLCrossReference} from "../CrossReference";

// Root queries specifies all the ways in which your GraphQl endpoint can return data
export default new GraphQLObjectType({


    name: 'RootQuery',
    description: 'Root level query resolvers',


    // Here, fields represents all the 'getter functions' we can call from our GraphQL endpoint to get data
    fields: () => ({



        Retailer: {

            type: new GraphQLList(GraphQlRetailer),
            // How should GraphQl get all users from our database? Query the Mongoose Model with no filter arguments
            resolve(parentValue, args) {
                 Retailer.findOne({});
            },
        },

            Medicine: {
               args:{Medicine_ID:{type: GraphQLInt}},
                type:  GraphQlMedicine,
                resolve(parentValue: any, args) {
                    return Medicine.findOne({Medicine_ID: {$eq: args.Medicine_ID}});
                }
            },

            AllMedicines:{
                type:  GraphQLCrossReference,
                args: {Retailer_ID: {type: GraphQLString}},
               async resolve(parentValue: any, args) {
                     return CrossReference.findOne({Retailer_ID: {$eq: args.Retailer_ID}});
                }
            }



    }),


});