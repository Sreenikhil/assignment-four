import {GraphQLInputObjectType, GraphQLString, GraphQLInt} from "graphql";


export const RetailerInput = new GraphQLInputObjectType({
    name: 'RetailerInput',
    description: 'Input fields for the Retailer model',
    fields: () => ({
        Company_name: { type: GraphQLString },
        Country_code: { type: GraphQLInt },
        Phone_Number: {type: GraphQLInt},
        Address: { type: GraphQLString },
        Country:{ type: GraphQLString },
        City:{ type: GraphQLString },
        Retailer_ID:{ type: GraphQLString },
        Password:{ type: GraphQLString },
    }),

})