import {GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList} from "graphql"
import {GraphQlMedicine} from "./Medicine";
import {CrossReference} from "../Models/CrossReferenceSchema";

// Graphql object that will represent our mongoose model
export const GraphQLCrossReference = new GraphQLObjectType({
    name: 'CrossReference',
    description: 'CrossReference of the website',
    // Fields represent what can GraphQl return from the User object
    fields: () => ({

        // There are no resolvers specified for the firstName or lastName field, GraphQL will attempt to get this
        // information bu performing user.firstName and user.lastName respectively, which will work!
        Retailer_ID:{type: GraphQLString},
        ID: {
            type: new GraphQLList(GraphQlMedicine),
            resolve(parent:any, args){
                return parent.ID
            }
        }
    })
})