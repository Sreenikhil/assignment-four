import {GraphQLSchema} from "graphql";
import RetailerMutation from "./Mutations/RetailerMutation";
import RetailerQuery from "./Query/RetailerQuery";

// Specify a GraphQL schema with our queries ('getters') and mutators ('setters')
export default new GraphQLSchema({
    query: RetailerQuery,
    mutation: RetailerMutation
});