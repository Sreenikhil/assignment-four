import mongoose = require('mongoose');

export interface IMedicineRequest  extends mongoose.Document{
       MedicineCount: number,
       MedicineID: number,
       RetailerID: string,
       Company_name: string,
       Notes: string,
       by: string
}