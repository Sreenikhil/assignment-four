"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var medicineSchema_1 = require("./medicineSchema");
// I want to make, medicine ID a number which can at most contain 7 digits, making the largest medicine id possible is 9999999.
// There are atmost 1 million unique medicine ID's possible.
var crossReferenceSchema = new mongoose.Schema({
    Retailer_ID: {
        type: String,
        required: true
    },
    ID: {
        type: [medicineSchema_1.medicineSchema]
    }
});
exports.CrossReference = mongoose.model('CrossReference', crossReferenceSchema);
