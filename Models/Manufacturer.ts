import mongoose = require('mongoose');
export interface IManufacturer extends mongoose.Document {
    Company_name: string,   // Ensure if it is not a number
    Country_code: number,
    Phone_Number: number,
    Address: string,
    Country: string,
    City: string,           // Ensure if it is not a number
    Manufacturer_ID: string,   // Check if it is greater than 3 characters
    Password: string       // Check if it is between length 3 - 16, has some special characters, uppercase, lowercase and a number.
}