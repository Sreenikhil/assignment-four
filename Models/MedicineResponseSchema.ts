import mongoose = require('mongoose');
import {IMedicineResponse} from "./MedicineResponse";

const MedicineResponseSchema  : mongoose.Schema<IMedicineResponse> = new mongoose.Schema<IMedicineResponse>({
    Duration_in_days: {
        type: Number,
        required: true,  // Built-In validation
    },

    Cost:{
        type: Number,
        required: true  // Built-In validation
    },
    Note:{
        type: String,
        required: true  // Built-In validation
    },

    MedicineID:{
        type: Number,
        required: true
    },

    RetailerID:{
        type: String,
        required: true
    },
    ManufacturerID:{
        type: String,
        required: true
    },

    by:{
        type: String,
        required: true
    }

})

export const MedicineResponse = mongoose.model<IMedicineResponse>('Response', MedicineResponseSchema);


