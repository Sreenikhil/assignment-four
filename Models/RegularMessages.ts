import mongoose = require('mongoose');

export interface IRegularMessages  extends mongoose.Document{
    Source: string,
    Destination: string,
    Message: string
}