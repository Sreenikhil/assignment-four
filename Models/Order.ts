import mongoose = require('mongoose');
export  interface IOrder extends mongoose.Document {
    Medicine_ID: number,
    count: number,
    Notes: string // Any thing, client wants to say with Manufacturer
}